﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using BombermanClone.Logic.Seged;
    using BombermanClone.Repository;
    using BombermanClone.Repository.Seged;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Logic tests
    /// </summary>
    [TestFixture]
    internal class LogicTests
    {
        private Mock<IRepository> mockedRepository;
        private IGameLogic logic;
        private IGameModel gameModel;
        private double skinWidth = 0.01;

        /// <summary>
        /// Sets up game enviroment
        /// </summary>
        [SetUp]
        public void Setup()
        {
            // minden mezot uresre állítunk
            MapTile[,] mapTiles = new MapTile[15, 11];
            for (int i = 0; i < 15; i++)
            {
                for (int j = 0; j < 11; j++)
                {
                    mapTiles[i, j] = new MapTile(MapTileType.Ground);
                }
            }

            // korbekeretine a palyat fallal
            for (int i = 0; i < 15; i++)
            {
                mapTiles[i, 0].TileType = MapTileType.Wall;
                mapTiles[i, 10].TileType = MapTileType.Wall;
            }

            for (int i = 0; i < 11; i++)
            {
                mapTiles[0, i].TileType = MapTileType.Wall;
                mapTiles[14, i].TileType = MapTileType.Wall;
            }

            // beálítunk kezdőpontokat
            List<Point> starterpoints = new List<Point>();
            starterpoints.Add(new Point(1, 1));
            starterpoints.Add(new Point(10, 10));

            Map map = new Map(mapTiles, starterpoints);
            this.mockedRepository = new Mock<IRepository>();
            this.mockedRepository.Setup(x => x.LoadMap()).Returns(map);

            this.gameModel = new GameModel();
            this.logic = new GameLogic(this.gameModel, this.mockedRepository.Object);
        }

        /// <summary>
        /// Tests if the player can move in the given direction with no walls
        /// </summary>
        [Test]
        public void CanMoveDownWhenTheresNoWall()
        {
            this.logic.StartGame(10, 10);

            double stepLength = 0.2;
            double originalYPosition = this.gameModel.Players[0].Position.Y;
            this.logic.MovePlayer(0, Direction.Down, stepLength);
            Assert.That((originalYPosition + stepLength) == this.gameModel.Players[0].Position.Y);
        }

        /// <summary>
        /// Tests if the player can move in the given direction with no walls
        /// </summary>
        [Test]
        public void CanMoveUpWhenTheresNoWall()
        {
            this.logic.StartGame(10, 10);
            this.gameModel.Players[0].Position = new Point(5, 5);

            double stepLength = 0.2;
            double originalYPosition = this.gameModel.Players[0].Position.Y;
            this.logic.MovePlayer(0, Direction.Up, stepLength);
            Assert.That((originalYPosition - stepLength) == this.gameModel.Players[0].Position.Y);
        }

        /// <summary>
        /// Tests if the player can move in the given direction with no walls
        /// </summary>
        [Test]
        public void CanMoveLeftWhenTheresNoWall()
        {
            this.logic.StartGame(10, 10);
            this.gameModel.Players[0].Position = new Point(5, 5);

            double stepLength = 0.2;
            double originalXPosition = this.gameModel.Players[0].Position.X;
            this.logic.MovePlayer(0, Direction.Left, stepLength);
            Assert.That((originalXPosition - stepLength) == this.gameModel.Players[0].Position.X);
        }

        /// <summary>
        /// Tests if the player can move in the given direction with no walls
        /// </summary>
        [Test]
        public void CanMoveRightWhenTheresNoWall()
        {
            this.logic.StartGame(10, 10);
            this.gameModel.Players[0].Position = new Point(5, 5);

            double stepLength = 0.2;
            double originalXPosition = this.gameModel.Players[0].Position.X;
            this.logic.MovePlayer(0, Direction.Right, stepLength);
            Assert.That((originalXPosition + stepLength) == this.gameModel.Players[0].Position.X);
        }

        /// <summary>
        /// Tests if the player move correctly to the edge of wall
        /// </summary>
        [Test]
        public void CanOnlyMoveDownToEdgeOfWall()
        {
            this.logic.StartGame(10, 10);
            this.gameModel.MatchMap.CurrentMapState[1, 2].TileType = MapTileType.Wall;

            double stepLength = 0.8;
            double originalYPosition = this.gameModel.Players[0].Position.Y;
            this.logic.MovePlayer(0, Direction.Down, stepLength);
            Assert.That(((originalYPosition + 1) - (this.gameModel.Players[0].PlayerSize + this.skinWidth)) == this.gameModel.Players[0].Position.Y);
        }

        /// <summary>
        /// Tests if the player move correctly to the edge of wall
        /// </summary>
        [Test]
        public void CanOnlyMoveLeftToEdgeOfWall()
        {
            this.logic.StartGame(10, 10);

            double stepLength = 0.8;
            double originalXPosition = this.gameModel.Players[0].Position.X;
            this.logic.MovePlayer(0, Direction.Left, stepLength);
            Assert.That((originalXPosition + this.skinWidth) == this.gameModel.Players[0].Position.X);
        }

        /// <summary>
        /// Tests if the player move correctly to the edge of wall
        /// </summary>
        [Test]
        public void CanOnlyMoveUpToEdgeOfWall()
        {
            this.logic.StartGame(10, 10);

            double stepLength = 0.8;
            double originalYPosition = this.gameModel.Players[0].Position.Y;
            this.logic.MovePlayer(0, Direction.Up, stepLength);
            Assert.That((originalYPosition + this.skinWidth) == this.gameModel.Players[0].Position.Y);
        }

        /// <summary>
        /// Tests if the player move correctly to the edge of wall
        /// </summary>
        [Test]
        public void CanOnlyMoveRightToEdgeOfWall()
        {
            this.logic.StartGame(10, 10);
            this.gameModel.MatchMap.CurrentMapState[2, 1].TileType = MapTileType.Wall;

            double stepLength = 0.8;
            double originalXPosition = this.gameModel.Players[0].Position.X;
            this.logic.MovePlayer(0, Direction.Right, stepLength);
            Assert.That(((originalXPosition + 1) - (this.gameModel.Players[0].PlayerSize + this.skinWidth)) == this.gameModel.Players[0].Position.X);
        }

        /// <summary>
        /// Tests that the player cant place two bombs at the same location
        /// </summary>
        [Test]
        public void CantPlaceMoreThanOneBombInTheSameLocation()
        {
            Point testPlayerPos = new Point(3, 3);

            this.logic.StartGame(10, 10);
            this.gameModel.Players[0].Position = testPlayerPos;
            this.gameModel.Players[0].MaxBombs = 10;
            this.logic.PlaceBomb(0);
            this.logic.PlaceBomb(0);
            Assert.That(this.gameModel.Players[0].BombsOnMap <= 1);
        }

        /// <summary>
        /// Tets if the player can set the maximum alowed amount of bombs
        /// </summary>
        [Test]
        public void CanPlaceMoreBombs_IfMaxBombsNotOne()
        {
            this.logic.StartGame(10, 10);
            this.gameModel.Players[0].MaxBombs = 10;
            Point testPlayerPos = new Point(3, 3);
            Point testPlayerPos2 = new Point(3, 4);

            this.gameModel.Players[0].Position = testPlayerPos;
            this.logic.PlaceBomb(0);
            this.gameModel.Players[0].Position = testPlayerPos2;
            this.logic.PlaceBomb(0);
            Assert.That(this.gameModel.Players[0].BombsOnMap >= 1);
        }
    }
}
