﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using BombermanClone.Logic.Seged;
    using BombermanClone.Repository;
    using BombermanClone.Repository.Seged;

    /// <summary>
    /// Handles bussines logic
    /// </summary>
    public class GameLogic : IGameLogic
    {
        private Random randomGenerator = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        ///
        /// </summary>
        /// <param name="gameModel">the model, to execute the logic functions on</param>
        /// <param name="gameRepository"> a repository </param>
        public GameLogic(IGameModel gameModel, IRepository gameRepository)
        {
            this.GameModel = gameModel;
            this.GameRepository = gameRepository;
        }

        /// <summary>
        /// Gets or sets the instance of a GameModel that is currently being used by logic
        /// </summary>
        public IGameModel GameModel { get; set; }

        /// <summary>
        /// Gets or sets the instance of a repository that is currently being used by logic
        /// </summary>
        public IRepository GameRepository { get; set; }

        /// <summary>
        /// Gets or sets the time between frames
        /// </summary>
        public double DeltaTime { get; set; }

        /// <summary>
        /// starts a game
        /// </summary>
        /// <param name="matchTime">length of match</param>
        /// <param name="matchRound">number of rounds</param>
        /// <param name="powerUpChance">chance to get poweup from wall</param>
        public void StartGame(int matchTime, int matchRound, double powerUpChance)
        {
            if (this.GameRepository != null)
            {
                if (this.GameModel != null)
                {
                    this.GameModel.MatchMap = this.GameRepository.LoadMap();
                }
                else
                {
                    throw new NotImplementedException(); // todo: noGameModelSelected exception
                }
            }
            else
            {
                throw new NotImplementedException(); // todo: noRepo exception
            }

            this.GameModel.MatchTime = matchTime;
            this.GameModel.Timer = matchTime;
            this.GameModel.MatchLength = matchRound;
            this.GameModel.PowerUpChance = powerUpChance;

            Player temp = new Player();
            temp.Position = this.GameModel.MatchMap.StartingPositions[0];
            this.GameModel.Players.Add(temp);

            temp = new Player();
            temp.Position = this.GameModel.MatchMap.StartingPositions[1];
            this.GameModel.Players.Add(temp);
        }

        /// <summary>
        /// Moves the given player in the requested direction, while also taking collision into account
        /// </summary>
        /// <param name="playerIndex">index of player</param>
        /// <param name="direction">direction to move in</param>
        /// <param name="stepLength">how big the movement should be each tick</param>
        public void MovePlayer(int playerIndex, Direction direction, double stepLength)
        {

            double playerPosX = this.GameModel.Players[playerIndex].Position.X;
            double playerPosY = this.GameModel.Players[playerIndex].Position.Y;
            double playerSize = this.GameModel.Players[playerIndex].PlayerSize;

            Rect playerPos = new Rect(playerPosX, playerPosY, playerSize, playerSize);

            int obstacleX = 0;
            int obstacleYTop = 0;
            int obstacleYBottom = 0;
            int obstacleXLeft = 0;
            int obstacleXRight = 0;
            int obstacleY = 0;
            double skinwidth = 0.01;

            // todo: Ha általánosítani tudnánk minden irányba az elenőrzést, azzal sok kódismétlést meglehetne spórolni
            switch (direction)
            {
                case Direction.Right:
                    // beálítjuk az akadály x kordinátáját a játékostól egyel jobbra lévő akadály x koordinátájára
                    obstacleX = (int)Math.Truncate(playerPosX) + 1;

                    obstacleYTop = (int)Math.Truncate(playerPos.TopLeft.Y);
                    obstacleYBottom = (int)Math.Truncate(playerPos.BottomLeft.Y);

                    // Ha a játékos két négyzetrácsba is belelóg, akkor eldöntjük, hogy melyiken van akadály és annak a négyzetnek a koordinátá
                    if (Math.Truncate(playerPos.TopLeft.Y) != Math.Truncate(playerPos.BottomLeft.Y))
                    {
                        if (this.GameModel.MatchMap.CurrentMapState[obstacleX, obstacleYTop].TileType != MapTileType.Ground)
                        {
                            obstacleY = obstacleYTop;
                        }
                        else if (this.GameModel.MatchMap.CurrentMapState[obstacleX, obstacleYBottom].TileType != MapTileType.Ground)
                        {
                            obstacleY = obstacleYBottom;
                        }
                        else
                        {
                            obstacleY = obstacleYTop;
                        }
                    }

                    // ha a játékos csak egy négyzetrácsba lóg bele, akkor annak a négyzetnek a Y koordinátáját beálítjuk az akadály y koordinátájának
                    else
                    {
                        obstacleY = (int)Math.Truncate(playerPos.Y);
                    }

                    // mozgatás

                    // ha akadály föld, akkor csak mozgunk az irányába
                    if (this.GameModel.MatchMap.CurrentMapState[obstacleX, obstacleY].TileType == MapTileType.Ground)
                    {
                        playerPosX += stepLength; // Ha a tile ahova mozogna járható, akkor léphet simán (feltételezzük, hogy egy lépésből nemtud több mint 1 tilét haladni)
                    }
                    else
                    {
                        if (playerPosX + playerSize + stepLength < obstacleX)
                        {
                            playerPosX += stepLength; // ha a játkos pozíciója + a lépéshosz nem nyúlik bele a falba, akkor lépjünk amekorát tudunk
                        }
                        else
                        {
                            playerPosX = obstacleX - 0.5 - skinwidth; // különben lépjünk akkorát, amekkora hely van
                        }
                    }

                    break;
                case Direction.Left:
                    // beálítjuk az akadály x kordinátáját a játékostól egyel jobbra lévő akadály x koordinátájára
                    obstacleX = (int)Math.Truncate(playerPosX) - 1;

                    // Ha a játékos két négyzetrácsba is belelóg, akkor eldöntjük, hogy melyiken van akadály és annak a négyzetnek a koordinátá
                    if (Math.Truncate(playerPos.TopLeft.Y) != Math.Truncate(playerPos.BottomLeft.Y))
                    {
                        obstacleYTop = (int)Math.Truncate(playerPos.TopLeft.Y);
                        obstacleYBottom = (int)Math.Truncate(playerPos.BottomLeft.Y);

                        if (this.GameModel.MatchMap.CurrentMapState[obstacleX, obstacleYTop].TileType != MapTileType.Ground)
                        {
                            obstacleY = obstacleYTop;
                        }
                        else if (this.GameModel.MatchMap.CurrentMapState[obstacleX, obstacleYBottom].TileType != MapTileType.Ground)
                        {
                            obstacleY = obstacleYBottom;
                        }
                        else
                        {
                            obstacleY = obstacleYTop;
                        }
                    }

                    // ha a játékos csak egy négyzetrácsba lóg bele, akkor annak a négyzetnek a Y koordinátáját beálítjuk az akadály y koordinátájának
                    else
                    {
                        obstacleY = (int)Math.Truncate(playerPos.Y);
                    }

                    // mozgatás

                    // ha akadály föld, akkor csak mozgunk az irányába
                    if (this.GameModel.MatchMap.CurrentMapState[obstacleX, obstacleY].TileType == MapTileType.Ground)
                    {
                        playerPosX += -stepLength; // Ha a tile ahova mozogna járható, akkor léphet simán (feltételezzük, hogy egy lépésből nemtud több mint 1 tilét haladni)
                    }
                    else
                    {
                        if (playerPosX - stepLength > obstacleX + 1)
                        {
                            playerPosX += -stepLength; // ha a játkos pozíciója + a lépéshosz nem nyúlik bele a falba, akkor lépjünk amekorát tudunk
                        }
                        else
                        {
                            playerPosX = obstacleX + 1 + skinwidth; // különben lépjünk akkorát, amekkora hely van
                        }
                    }

                    break;
                case Direction.Up:

                    obstacleY = (int)Math.Truncate(playerPosY) - 1;

                    // Ha a játékos két négyzetrácsba is belelóg, akkor eldöntjük, hogy melyiken van akadály és annak a négyzetnek a koordinátá
                    if (Math.Truncate(playerPos.TopLeft.X) != Math.Truncate(playerPos.TopRight.X))
                    {
                        obstacleXLeft = (int)Math.Truncate(playerPos.Left);
                        obstacleXRight = (int)Math.Truncate(playerPos.Right);

                        if (this.GameModel.MatchMap.CurrentMapState[obstacleXLeft, obstacleY].TileType != MapTileType.Ground)
                        {
                            obstacleX = obstacleXLeft;
                        }
                        else if (this.GameModel.MatchMap.CurrentMapState[obstacleXRight, obstacleY].TileType != MapTileType.Ground)
                        {
                            obstacleX = obstacleXRight;
                        }
                        else
                        {
                            obstacleX = obstacleXLeft;
                        }
                    }

                    // ha a játékos csak egy négyzetrácsba lóg bele, akkor annak a négyzetnek a Y koordinátáját beálítjuk az akadály y koordinátájának
                    else
                    {
                        obstacleX = (int)Math.Truncate(playerPos.X);
                    }

                    // mozgatás

                    // ha akadály föld, akkor csak mozgunk az irányába
                    if (this.GameModel.MatchMap.CurrentMapState[obstacleX, obstacleY].TileType == MapTileType.Ground)
                    {
                        playerPosY += -stepLength; // Ha a tile ahova mozogna járható, akkor léphet simán (feltételezzük, hogy egy lépésből nemtud több mint 1 tilét haladni)
                    }
                    else
                    {
                        if (playerPosY - stepLength > obstacleY + 1)
                        {
                            playerPosY += -stepLength; // ha a játkos pozíciója + a lépéshosz nem nyúlik bele a falba, akkor lépjünk amekorát tudunk
                        }
                        else
                        {
                            playerPosY = obstacleY + 1 + skinwidth; // különben lépjünk akkorát, amekkora hely van
                        }
                    }

                    break;
                case Direction.Down:

                    obstacleY = (int)Math.Truncate(playerPosY) + 1;

                    // Ha a játékos két négyzetrácsba is belelóg, akkor eldöntjük, hogy melyiken van akadály és annak a négyzetnek a koordinátá
                    if (Math.Truncate(playerPos.TopLeft.X) != Math.Truncate(playerPos.TopRight.X))
                    {
                        obstacleXLeft = (int)Math.Truncate(playerPos.Left);
                        obstacleXRight = (int)Math.Truncate(playerPos.Right);

                        if (this.GameModel.MatchMap.CurrentMapState[obstacleXLeft, obstacleY].TileType != MapTileType.Ground)
                        {
                            obstacleX = obstacleXLeft;
                        }
                        else if (this.GameModel.MatchMap.CurrentMapState[obstacleXRight, obstacleY].TileType != MapTileType.Ground)
                        {
                            obstacleX = obstacleXRight;
                        }
                        else
                        {
                            obstacleX = obstacleXLeft;
                        }
                    }

                    // ha a játékos csak egy négyzetrácsba lóg bele, akkor annak a négyzetnek a Y koordinátáját beálítjuk az akadály y koordinátájának
                    else
                    {
                        obstacleX = (int)Math.Truncate(playerPos.X);
                    }

                    // mozgatás

                    // ha akadály föld, akkor csak mozgunk az irányába
                    if (this.GameModel.MatchMap.CurrentMapState[obstacleX, obstacleY].TileType == MapTileType.Ground)
                    {
                        playerPosY += stepLength; // Ha a tile ahova mozogna járható, akkor léphet simán (feltételezzük, hogy egy lépésből nemtud több mint 1 tilét haladni)
                    }
                    else
                    {
                        if (playerPosY + playerSize + stepLength < obstacleY)
                        {
                            playerPosY += stepLength; // ha a játkos pozíciója + a lépéshosz nem nyúlik bele a falba, akkor lépjünk amekorát tudunk
                        }
                        else
                        {
                            playerPosY = obstacleY - 0.5 - skinwidth; // különben lépjünk akkorát, amekkora hely van
                        }
                    }

                    break;
                default:
                    break;
            }

            this.GameModel.Players[playerIndex].Position = new Point(playerPosX, playerPosY);
        }

        /// <summary>
        /// Called each frame, while the game is running
        /// </summary>
        public void OneTick()
        {
            // throw new NotImplementedException();
            this.ProcesBombs();
            this.ProccesBlasts();
            this.PlayerInteraction();
            this.GameModel.Timer -= this.DeltaTime;
            this.CheckForWinner();
        }

        /// <summary>
        /// Places a bomb on the map
        /// </summary>
        /// <param name="playerIndex"> The index of the player, that palces the bomb</param>
        public void PlaceBomb(int playerIndex)
        {
            Player bombPlacer = this.GameModel.Players[playerIndex];

            if (bombPlacer.BombsOnMap < bombPlacer.MaxBombs && this.GameModel.MapObjects[(int)Math.Round(bombPlacer.Position.X), (int)Math.Round(bombPlacer.Position.Y)] == null)
            {
                Bomb bomb = new Bomb(bombPlacer);

                bomb.Player.BombsOnMap++;

                this.GameModel.Bombs.Add(new EntityLifeTime<Bomb>(bombPlacer.BombExplosionTime, bomb));
                this.GameModel.MapObjects[(int)Math.Round(bomb.Player.Position.X), (int)Math.Round(bomb.Player.Position.Y)] = bomb;
            }
        }

        /// <summary>
        /// Call on each frame, to procces bomb logic (explosion time)
        /// </summary>
        private void ProcesBombs()
        {
            // visszafele járjuk be, hogy eltudjunk távolítani elemeket iterálás közben
            for (int i = this.GameModel.Bombs.Count() - 1; i >= 0; i--)
            {
                if (this.GameModel.Bombs[i].Current < this.GameModel.Bombs[i].Max)
                {
                    this.GameModel.Bombs[i].Current += this.DeltaTime;
                }
                else
                {
                    this.GameModel.MapObjects[(int)this.GameModel.Bombs[i].Entity.Position.X, (int)this.GameModel.Bombs[i].Entity.Position.Y] = null;
                    this.GameModel.Bombs[i].Entity.Player.BombsOnMap--;

                    this.ExplodeBomb(this.GameModel.Bombs[i].Entity);

                    this.GameModel.Bombs.Remove(this.GameModel.Bombs[i]);
                }
            }
        }

        private void ProccesBlasts()
        {
            // visszafele járjuk be, hogy eltudjunk távolítani elemeket iterálás közben
            for (int i = this.GameModel.Blasts.Count() - 1; i >= 0; i--)
            {
                if (this.GameModel.Blasts[i].Current < this.GameModel.Blasts[i].Max)
                {
                    this.GameModel.Blasts[i].Current += this.DeltaTime;
                }
                else
                {
                    this.GameModel.BlastMap[(int)this.GameModel.Blasts[i].Entity.Position.X, (int)this.GameModel.Blasts[i].Entity.Position.Y] = null;

                    this.GameModel.Blasts.Remove(this.GameModel.Blasts[i]);
                }
            }
        }

        /// <summary>
        /// Makes a bomb explode
        /// </summary>
        /// <param name="bomb">the bomb, that explodes</param>
        private void ExplodeBomb(Bomb bomb)
        {
            Player bombPlacer = bomb.Player;

            int bombX = (int)Math.Round(bomb.Position.X);
            int bombY = (int)Math.Round(bomb.Position.Y);

            bool[] robbant = new bool[4]; // egy irányba csak egy falat robbanthat fel
            for (int i = 1; i <= bombPlacer.BombPower; i++)
            {
                Blast tempBlast = new Blast(bombX, bombY);

                this.GameModel.Blasts.Add(new EntityLifeTime<Blast>(bombPlacer.BlastTime, tempBlast));
                this.GameModel.BlastMap[bombX, bombY] = tempBlast;

                // bombától jobbra
                if (!robbant[0] && bombX + i >= 0 && bombX + i <= this.GameModel.MatchMap.CurrentMapState.GetLength(0) - 1)
                {
                    if (!robbant[0] && this.GameModel.MatchMap.CurrentMapState[bombX + i, bombY].TileType == MapTileType.DestructibleWall)
                    {
                        this.GameModel.MatchMap.CurrentMapState[bombX + i, bombY].TileType = MapTileType.Ground;
                        this.PlaceEntity(new PowerUp(bombX + i, bombY, this.SelectPowerUp()), this.GameModel.PowerUpChance);
                        robbant[0] = true;
                    }

                    if (this.GameModel.MatchMap.CurrentMapState[bombX + i, bombY].TileType != MapTileType.Wall)
                    {
                        tempBlast = new Blast(bombX + i, bombY);
                        this.GameModel.Blasts.Add(new EntityLifeTime<Blast>(bombPlacer.BlastTime, tempBlast));
                        this.GameModel.BlastMap[bombX + i, bombY] = tempBlast;
                    }
                    else
                    {
                        robbant[0] = true;
                    }
                }

                // bombától balra
                if (!robbant[1] && bombX - i >= 0 && bombX - i <= this.GameModel.MatchMap.CurrentMapState.GetLength(0) - 1)
                {
                    if (!robbant[1] && this.GameModel.MatchMap.CurrentMapState[bombX - i, bombY].TileType == MapTileType.DestructibleWall)
                    {
                        this.GameModel.MatchMap.CurrentMapState[bombX - i, bombY].TileType = MapTileType.Ground;
                        this.PlaceEntity(new PowerUp(bombX - i, bombY, this.SelectPowerUp()), this.GameModel.PowerUpChance);
                        robbant[1] = true;
                    }

                    if (this.GameModel.MatchMap.CurrentMapState[bombX - i, bombY].TileType != MapTileType.Wall)
                    {
                        tempBlast = new Blast(bombX - i, bombY);
                        this.GameModel.Blasts.Add(new EntityLifeTime<Blast>(bombPlacer.BlastTime, tempBlast));
                        this.GameModel.BlastMap[bombX - i, bombY] = tempBlast;
                    }
                    else
                    {
                        robbant[1] = true;
                    }
                }

                // bombától feljebb
                if (!robbant[2] && bombY + i >= 0 && bombY + i <= this.GameModel.MatchMap.CurrentMapState.GetLength(1) - 1)
                {
                    if (!robbant[2] && this.GameModel.MatchMap.CurrentMapState[bombX, bombY + i].TileType == MapTileType.DestructibleWall)
                    {
                        this.PlaceEntity(new PowerUp(bombX, bombY + i, this.SelectPowerUp()), this.GameModel.PowerUpChance);
                        this.GameModel.MatchMap.CurrentMapState[bombX, bombY + i].TileType = MapTileType.Ground;
                        robbant[2] = true;
                    }

                    if (this.GameModel.MatchMap.CurrentMapState[bombX, bombY + i].TileType != MapTileType.Wall)
                    {
                        tempBlast = new Blast(bombX, bombY + i);
                        this.GameModel.Blasts.Add(new EntityLifeTime<Blast>(bombPlacer.BlastTime, tempBlast));
                        this.GameModel.BlastMap[bombX, bombY + i] = tempBlast;
                    }
                    else
                    {
                        robbant[2] = true;
                    }
                }

                // bombától lejjebb
                if (!robbant[3] && bombY - i >= 0 && bombY - i <= this.GameModel.MatchMap.CurrentMapState.GetLength(1) - 1)
                {
                    if (!robbant[3] && this.GameModel.MatchMap.CurrentMapState[bombX, bombY - i].TileType == MapTileType.DestructibleWall)
                    {
                        this.PlaceEntity(new PowerUp(bombX, bombY - i, this.SelectPowerUp()), this.GameModel.PowerUpChance);
                        this.GameModel.MatchMap.CurrentMapState[bombX, bombY - i].TileType = MapTileType.Ground;
                        robbant[3] = true;
                    }

                    if (this.GameModel.MatchMap.CurrentMapState[bombX, bombY - i].TileType != MapTileType.Wall)
                    {
                        tempBlast = new Blast(bombX, bombY - i);

                        this.GameModel.Blasts.Add(new EntityLifeTime<Blast>(bombPlacer.BlastTime, tempBlast));
                        this.GameModel.BlastMap[bombX, bombY - i] = tempBlast;
                    }
                    else
                    {
                        robbant[3] = true;
                    }
                }
            }
        }

        private void CheckForWinner()
        {
            if (this.GameModel.Timer < 0)
            {
                this.EndRound(-1);
            }

            int eletben = this.GameModel.Players.Count();

            foreach (var item in this.GameModel.Players)
            {
                Rect playerTemp = new Rect(item.Position.X, item.Position.Y, item.PlayerSize, item.PlayerSize);

                // Ha akármelyik oldal belelóg, akkor meghal
                if (this.GameModel.BlastMap[(int)item.Position.X, (int)playerTemp.Top] != null)
                {
                    eletben--;
                    item.Alive = false;
                }
                else if (this.GameModel.BlastMap[(int)item.Position.X, (int)playerTemp.Bottom] != null)
                {
                    eletben--;
                    item.Alive = false;
                }
                else if (this.GameModel.BlastMap[(int)playerTemp.Left, (int)item.Position.Y] != null)
                {
                    eletben--;
                    item.Alive = false;
                }
                else if (this.GameModel.BlastMap[(int)playerTemp.Right, (int)item.Position.Y] != null)
                {
                    eletben--;
                    item.Alive = false;
                }
            }

            if (eletben <= 1)
            {
                if (eletben == 0)
                {
                    this.EndRound(-1);
                }
                else
                {
                    for (int i = 0; i < this.GameModel.Players.Count; i++)
                    {
                        if (this.GameModel.Players[i].Alive == true)
                        {
                            this.EndRound(i);
                            break;
                        }
                    }
                }
            }
        }

        private void EndRound(int winnerId)
        {
            if (winnerId == -1)
            {
                this.ResetMatch();
            }
            else
            {
                this.GameModel.Scores[winnerId]++;
                this.ResetMatch();
            }
        }

        private void ResetMatch()
        {
            // visszamásoljuk a jelenlegi állapotba, az eredeti állapotot
            for (int i = 0; i < this.GameModel.MatchMap.OriginalMapState.GetLength(0); i++)
            {
                for (int j = 0; j < this.GameModel.MatchMap.OriginalMapState.GetLength(1); j++)
                {
                    this.GameModel.MatchMap.CurrentMapState[i, j].TileType = this.GameModel.MatchMap.OriginalMapState[i, j].TileType;
                }
            }

            Array.Clear(this.GameModel.BlastMap, 0, this.GameModel.BlastMap.Length);
            Array.Clear(this.GameModel.MapObjects, 0, this.GameModel.MapObjects.Length);
            this.GameModel.Bombs.Clear();
            this.GameModel.Blasts.Clear();

            for (int i = 0; i < this.GameModel.Players.Count; i++)
            {
                this.GameModel.Players[i].Position = this.GameModel.MatchMap.StartingPositions[i];
                this.GameModel.Players[i].Alive = true;
                this.GameModel.Players[i].BombPower = 1;
                this.GameModel.Players[i].BombsOnMap = 0;
            }

            this.GameModel.Timer = this.GameModel.MatchTime;
        }

        private void PlaceEntity(MapEntity placedObject, double chance = 1)
        {
            if (this.randomGenerator.NextDouble() < chance)
            {
                this.GameModel.MapObjects[(int)placedObject.Position.X, (int)placedObject.Position.Y] = placedObject;
                this.GameModel.InteractableEntities.Add(placedObject);
            }
        }

        private PowerUpTypes SelectPowerUp()
        {
            return (PowerUpTypes)this.randomGenerator.Next(1, Enum.GetNames(typeof(PowerUpTypes)).Length + 1);
        }

        private void PlayerInteraction()
        {
            foreach (var item in this.GameModel.Players)
            {
                if (this.GameModel.MapObjects[(int)item.Position.X, (int)item.Position.Y] == null)
                {
                    continue;
                }

                if (this.GameModel.MapObjects[(int)item.Position.X, (int)item.Position.Y].GetType() == typeof(PowerUp))
                {
                    item.BombPower++;
                    this.GameModel.MapObjects[(int)item.Position.X, (int)item.Position.Y] = null;
                    break;
                }
            }
        }
    }
}
