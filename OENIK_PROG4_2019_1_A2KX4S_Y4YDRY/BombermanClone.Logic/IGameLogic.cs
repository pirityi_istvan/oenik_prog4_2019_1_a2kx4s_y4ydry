﻿// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BombermanClone.Logic.Seged;
    using BombermanClone.Repository;

    /// <summary>
    /// handles bussines logic
    /// </summary>
   public interface IGameLogic
    {
        /// <summary>
        /// Gets or sets the instance of a GameModel that is currently being used by logic
        /// </summary>
        IGameModel GameModel { get; set; }

        /// <summary>
        /// Gets or sets the instance of a repository that is currently being used by logic
        /// </summary>
        IRepository GameRepository { get; set; }

        /// <summary>
        /// Gets or sets time between frames
        /// </summary>
        double DeltaTime { get; set; }

        /// <summary>
        /// Starts a game
        /// </summary>
        /// <param name="matchTime">the length of a match</param>
        /// <param name="matchRound">how many rounds in a match</param>
        /// <param name="powerUpChance">chance to get a power up from walls</param>
        void StartGame(int matchTime, int matchRound, double powerUpChance = 0.1);

        /// <summary>
        /// Moves the playyer in the given direction
        /// </summary>
        /// <param name="playerIndex">the index of the player that you wish to move</param>
        /// <param name="direction">direction to move in</param>
        /// <param name="stepLength">how big the movement should be each tick</param>
        void MovePlayer(int playerIndex, Direction direction, double stepLength); // Esetleg enumot lehet nem kéne használni vagy máshova kéne tenni

        /// <summary>
        /// Places a bomb
        /// </summary>
        /// <param name="playerIndex">The player that placed the bomb</param>
        void PlaceBomb(int playerIndex);

        /// <summary>
        /// Called each frame, while the game is running
        /// </summary>
        void OneTick();
    }
}
