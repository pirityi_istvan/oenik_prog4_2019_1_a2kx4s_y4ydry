﻿// <copyright file="CustomDataTypes.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Logic.Seged
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// 4 way direction
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// right
        /// </summary>
        Right,

        /// <summary>
        /// left
        /// </summary>
        Left,

        /// <summary>
        /// up
        /// </summary>
        Up,

        /// <summary>
        /// down
        /// </summary>
        Down
    }
}
