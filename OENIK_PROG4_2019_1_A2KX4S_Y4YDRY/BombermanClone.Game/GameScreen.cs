﻿// <copyright file="GameScreen.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG4_2019_1_A2KX4S_Y4YDRY
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using BombermanClone.Logic;
    using BombermanClone.Logic.Seged;
    using BombermanClone.Repository;
    using BombermanClone.Repository.Seged;

    /// <summary>
    /// Wpf framework element that handles drawing, and inputs
    /// </summary>
    internal class GameScreen : FrameworkElement
    {
        private IGameModel model;
        private IGameLogic logic;
        private IRepository repository;
        private ImageBrush[] tileImages;
        private ImageBrush[] objectImages;

        // ImageBrush[] bomb;
        private double width = 1000;
        private double height = 1000;
        private double uiHeight;
        private double deltaTime = 15;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameScreen"/> class.
        ///
        /// </summary>
        public GameScreen()
        {
            this.tileImages = new ImageBrush[Enum.GetNames(typeof(MapTileType)).Length];
            this.objectImages = new ImageBrush[3];

            for (int i = 0; i < this.tileImages.Length; i++)
            {
                this.tileImages[i] = new ImageBrush(new BitmapImage(new Uri("Graphics/Tiles/" + i + ".png", UriKind.Relative)));
            }

            for (int i = 0; i < this.objectImages.Length; i++)
            {
                this.objectImages[i] = new ImageBrush(new BitmapImage(new Uri("Graphics/Objects/" + i + ".png", UriKind.Relative)));
            }

            this.model = new GameModel();
            this.repository = new GameRepository();

            this.Loaded += this.GameScreen_Loaded;
            this.Initialized += this.GameScreen_Initialized;
            this.uiHeight = this.height / 10;
        }

        /// <summary>
        /// Handles element rendering
        /// </summary>
        /// <param name="drawingContext">the drawingContext</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            double tileWidth = this.width / 15;
            double tileHeight = this.height / 11;

            drawingContext.DrawRectangle(Brushes.Black, null, new Rect(0, 0, this.width, this.uiHeight));

            drawingContext.DrawText(
                new FormattedText(
                    this.model.Scores[0].ToString(),
                CultureInfo.GetCultureInfo("en-us"),
                FlowDirection.LeftToRight,
                new Typeface("Verdana"),
                36,
                System.Windows.Media.Brushes.White),
                new System.Windows.Point(0, 0));

                        drawingContext.DrawText(
                new FormattedText(
                    this.model.Scores[1].ToString(),
                CultureInfo.GetCultureInfo("en-us"),
                FlowDirection.RightToLeft,
                new Typeface("Verdana"),
                36,
                System.Windows.Media.Brushes.White),
                new System.Windows.Point(this.width, 0));

                                    drawingContext.DrawText(
                new FormattedText(
                    ((int)this.model.Timer).ToString(),
                CultureInfo.GetCultureInfo("en-us"),
                FlowDirection.LeftToRight,
                new Typeface("Verdana"),
                36,
                System.Windows.Media.Brushes.White),
                new System.Windows.Point(this.width / 2, 0));

            // tiles kirajzolása
            for (int i = 0; i < 11; i++)
            {
                for (int j = 0; j < 15; j++)
                {
                    Point center = new Point(tileWidth * j, (tileHeight * i) + this.uiHeight);
                    Rect tile = new Rect(center.X, center.Y, tileWidth, tileHeight);
                    Point position = new Point(tileWidth * j, tileHeight * i);

                    switch (this.model.MatchMap.CurrentMapState[j, i].TileType)
                    {
                        case MapTileType.Ground:
                            drawingContext.DrawRectangle(this.tileImages[(int)MapTileType.Ground], null, tile);
                            break;
                        case MapTileType.DestructibleWall:
                            drawingContext.DrawRectangle(this.tileImages[(int)MapTileType.DestructibleWall], null, tile);
                            break;
                        case MapTileType.Wall:
                            drawingContext.DrawRectangle(this.tileImages[(int)MapTileType.Wall], null, tile);
                            break;
                        default:
                            break;
                    }
                }
            }

            // entityk kirajzolása
            for (int i = 0; i < 11; i++)
            {
                for (int j = 0; j < 15; j++)
                {
                    if (this.model.MapObjects[j, i] != null)
                    {
                        Point center = new Point(tileWidth * j, (tileHeight * i) + this.uiHeight);
                        Rect tile = new Rect(center.X, center.Y, tileWidth, tileHeight);

                        if (this.model.MapObjects[j, i] != null)
                        {
                            drawingContext.DrawRectangle(this.objectImages[this.model.MapObjects[j, i].Imageid], null, tile);
                        }
                    }
                }
            }

            // robbanás kirajzolása
            for (int i = 0; i < 11; i++)
            {
                for (int j = 0; j < 15; j++)
                {
                    if (this.model.BlastMap[j, i] != null)
                    {
                        Point center = new Point(tileWidth * j, (tileHeight * i) + this.uiHeight);
                        Rect tile = new Rect(center.X, center.Y, tileWidth, tileHeight);

                        if (this.model.BlastMap[j, i] != null)
                        {
                            drawingContext.DrawRectangle(this.objectImages[this.model.BlastMap[j, i].Imageid], null, tile);
                        }
                    }
                }
            }

            // játékos kirajzolása
            foreach (var item in this.model.Players)
            {
                Point center = new Point(tileWidth * item.Position.X, (tileHeight * item.Position.Y) + this.uiHeight);
                Rect tile = new Rect(center.X, center.Y, tileWidth * item.PlayerSize, tileHeight * item.PlayerSize);

                drawingContext.DrawRectangle(Brushes.Crimson, null, tile);
            }
        }

        private void GameScreen_Initialized(object sender, EventArgs e)
        {
            this.logic = new GameLogic(this.model, this.repository);
            this.logic.DeltaTime = this.deltaTime / 1000; // beálítjuk, hogy milyen időközönként frissít a logic(másodperc)
            this.logic.StartGame(120, 5);

            this.width = this.Width;
            this.height = this.Height;
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(this.deltaTime);
            timer.Tick += this.Timer_Tick;
            timer.Start();
        }

        private void HandleInput()
        {
            if (Keyboard.IsKeyDown(Key.LeftShift))
            {
                this.logic.PlaceBomb(0);
            }

            if (Keyboard.IsKeyDown(Key.RightCtrl))
            {
                this.logic.PlaceBomb(1);
            }

            if (Keyboard.IsKeyDown(Key.W))
            {
                this.logic.MovePlayer(0, Direction.Up, 0.1);
            }

            if (Keyboard.IsKeyDown(Key.S))
            {
                this.logic.MovePlayer(0, Direction.Down, 0.1);
            }

            if (Keyboard.IsKeyDown(Key.A))
            {
                this.logic.MovePlayer(0, Direction.Left, 0.1);
            }

            if (Keyboard.IsKeyDown(Key.D))
            {
                this.logic.MovePlayer(0, Direction.Right, 0.1);
            }

            if (Keyboard.IsKeyDown(Key.Up))
            {
                this.logic.MovePlayer(1, Direction.Up, 0.1);
            }

            if (Keyboard.IsKeyDown(Key.Down))
            {
                this.logic.MovePlayer(1, Direction.Down, 0.1);
            }

            if (Keyboard.IsKeyDown(Key.Left))
            {
                this.logic.MovePlayer(1, Direction.Left, 0.1);
            }

            if (Keyboard.IsKeyDown(Key.Right))
            {
                this.logic.MovePlayer(1, Direction.Right, 0.1);
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            this.HandleInput();
            this.logic.OneTick();
            this.InvalidateVisual();
        }

        private void GameScreen_Loaded(object sender, RoutedEventArgs e)
        {
            Window window = Window.GetWindow(this);

            // Azért kell, hogy design időben ne dobjon kivételt
            if (window != null)
            {
            }
        }
    }
}
