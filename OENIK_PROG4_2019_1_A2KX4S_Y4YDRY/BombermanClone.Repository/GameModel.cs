﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BombermanClone.Repository.Seged;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// The object containing an instance of a game
    /// </summary>
    public class GameModel : IGameModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        ///
        /// </summary>
        public GameModel()
        {
            this.Players = new List<Player>();

            // MapObjects = new MapEntity[11, 15];
            this.MapObjects = new MapEntity[15, 11];
            this.BlastMap = new MapEntity[15, 11];
            this.Bombs = new List<EntityLifeTime<Bomb>>();
            this.Blasts = new List<EntityLifeTime<Blast>>();
            this.InteractableEntities = new List<MapEntity>();
            this.Scores = new int[2];
        }

        /// <summary>
        /// Gets or sets the size of each graphic
        /// </summary>
        public double TileSize { get; set; }

        /// <summary>
        /// Gets or sets the map and the starting positions
        /// </summary>
        public Map MatchMap { get; set; }

        /// <summary>
        /// Gets or sets objects on the map stored in a matrix
        /// </summary>
        public MapEntity[,] MapObjects { get; set; }

        /// <summary>
        /// Gets or sets blasts stored in a matrix
        /// </summary>
        public MapEntity[,] BlastMap { get; set; }

        /// <summary>
        /// Gets or sets bombs on the map. They are removed after a time
        /// </summary>
        public List<EntityLifeTime<Bomb>> Bombs { get; set; }

        /// <summary>
        /// Gets or sets blasts on the map. They are removed after a time
        /// </summary>
        public List<EntityLifeTime<Blast>> Blasts { get; set; }

        /// <summary>
        /// Gets or sets list of objects that the player can interact with
        /// </summary>
        public List<MapEntity> InteractableEntities { get; set; }

        /// <summary>
        /// Gets or sets the time
        /// </summary>
        public double Timer { get; set; }

        /// <summary>
        /// Gets or sets the Length of the game in time
        /// </summary>
        public int MatchTime { get; set; }

        /// <summary>
        /// Gets or sets score of each player
        /// </summary>
        public int[] Scores { get; set; }

        /// <summary>
        /// Gets or sets the length of the match in rounds
        /// </summary>
        public int MatchLength { get; set; }

        /// <summary>
        /// Gets or sets the current round
        /// </summary>
        public int CurrentRound { get; set; }

        /// <summary>
        /// Gets or sets the chance to spawn a powerup from the wall
        /// </summary>
        public double PowerUpChance { get; set; }

        /// <summary>
        /// Gets or sets the list of players
        /// </summary>
        public IList<Player> Players { get; set; }
    }
}
