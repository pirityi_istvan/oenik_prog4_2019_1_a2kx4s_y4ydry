﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BombermanClone.Repository.Seged;

    /// <summary>
    /// Interface for an instance of a game. Stores the current state of a game
    /// </summary>
    public interface IGameModel
    {
        // todo: Ezeknél a tömböknél valahogy megkell majd oldani, hogy frissüljenek, meg, kikel találni, hogy legyenek megjelenítve

        /// <summary>
        /// Gets or sets 2d array, that used, to represent the map, that is loaded from the repository
        /// </summary>
        Map MatchMap { get; set; }

        /// <summary>
        /// Gets or sets how big each tile should be
        /// </summary>
        double TileSize { get; set; }

        /// <summary>
        /// Gets or sets 2d array of objects on the map. It is recommended to have the same dimensions, as the map
        /// </summary>
        MapEntity[,] MapObjects { get; set; }

        /// <summary>
        /// Gets or sets 2d array of blasts on the map. It is recommended to have the same dimensions, as the map
        /// </summary>
        MapEntity[,] BlastMap { get; set; }

        /// <summary>
        /// Gets or sets the bombs on the map
        /// </summary>
        List<EntityLifeTime<Bomb>> Bombs { get; set; }

        /// <summary>
        /// Gets or sets the blasts on the map
        /// </summary>
        List<EntityLifeTime<Blast>> Blasts { get; set; }

        /// <summary>
        /// Gets or sets the entities the player can interact with
        /// </summary>
        List<MapEntity> InteractableEntities { get; set; }

        /// <summary>
        /// Gets or sets the current remaining time in the round
        /// </summary>
        double Timer { get; set; }

        /// <summary>
        /// Gets or sets the math time
        /// </summary>
        int MatchTime { get; set; }

        /// <summary>
        /// Gets or sets the current score, for each player
        /// </summary>
        int[] Scores { get; set; }

        /// <summary>
        /// Gets or sets the chance for a powerup to spawn from a wall
        /// </summary>
        double PowerUpChance { get; set; }

        /// <summary>
        /// Gets or sets the match length
        /// </summary>
        int MatchLength { get; set; }

        /// <summary>
        /// Gets or sets the current round
        /// </summary>
        int CurrentRound { get; set; }

        /// <summary>
        /// Gets or sets list of players
        /// </summary>
        IList<Player> Players { get; set; }
    }
}
