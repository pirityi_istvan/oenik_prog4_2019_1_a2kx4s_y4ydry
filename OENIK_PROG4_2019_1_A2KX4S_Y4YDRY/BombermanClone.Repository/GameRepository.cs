﻿// <copyright file="GameRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using BombermanClone.Repository.Seged;

    /// <summary>
    /// A repository of the game
    /// </summary>
    public class GameRepository : IRepository
    {
        /// <summary>
        /// Loads a map
        /// </summary>
        /// <returns>a map</returns>
        public Map LoadMap()
        {
            return this.LoadMapFromFile();
        }

        private Map LoadMapFromFile()
        {
            MapTile[,] map = new MapTile[11, 15];
            List<Point> startingPoints = new List<Point>();
            StreamReader sr = new StreamReader("map.txt");
            try
            {
                int i = 0;
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    for (int j = 0; j < line.Length; j++)
                    {
                        switch (line[j])
                        {
                            case '3':
                                map[i, j] = new MapTile(MapTileType.Wall);
                                break;
                            case '2':
                                map[i, j] = new MapTile(MapTileType.DestructibleWall);
                                break;
                            case '1':
                                map[i, j] = new MapTile(MapTileType.Ground);
                                break;
                            case '0':
                                map[i, j] = new MapTile(MapTileType.Ground);
                                startingPoints.Add(new Point(j, i));
                                break;
                            default:
                                break;
                        }
                    }

                    i++;
                }
            }
            finally
            {
                sr.Close();
            }

            return new Map(map, startingPoints);
        }
    }
}
