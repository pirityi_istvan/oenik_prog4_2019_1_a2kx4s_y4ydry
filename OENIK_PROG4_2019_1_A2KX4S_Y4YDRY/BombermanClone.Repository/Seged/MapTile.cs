﻿// <copyright file="MapTile.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository.Seged
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Tiles, that make up a map
    /// </summary>
    public class MapTile : ObservableObject
    {
        private MapTileType tileType;
        private int imageid;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapTile"/> class.
        ///
        /// </summary>
        /// <param name="tileType">the type of the tile</param>
        public MapTile(MapTileType tileType)
        {
            this.tileType = tileType;
        }

        /// <summary>
        /// Gets or sets the type of the tyle
        /// </summary>
        public MapTileType TileType
        {
            get { return this.tileType; }
            set { this.Set(ref this.tileType, value); }
        }

        /// <summary>
        /// Gets or sets the id of the tiles image
        /// </summary>
        public int Imageid
        {
            get { return this.imageid; }
            set { this.Set(ref this.imageid, value); }
        }
    }
}
