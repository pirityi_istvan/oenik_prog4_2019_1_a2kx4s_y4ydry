﻿// <copyright file="Blast.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository.Seged
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// The blast
    /// </summary>
    public class Blast : MapEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Blast"/> class.
        ///
        /// </summary>
        /// <param name="x">x axis</param>
        /// <param name="y">y axis</param>
        public Blast(double x, double y)
        {
            this.Position = new Point(x, y);
            this.Imageid = 1;
        }
    }
}
