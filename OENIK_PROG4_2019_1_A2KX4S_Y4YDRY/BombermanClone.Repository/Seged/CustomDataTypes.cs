﻿// <copyright file="CustomDataTypes.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository.Seged
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Types of tiles
    /// </summary>
    public enum MapTileType
    {
        /// <summary>
        /// Walkable tile
        /// </summary>
        Ground,

        /// <summary>
        /// wall that can be destroyed
        /// </summary>
        ///
        DestructibleWall,

        /// <summary>
        /// unbreakable wall
        /// </summary>
        Wall
    }

    /// <summary>
    /// The types of powerups
    /// </summary>
    public enum PowerUpTypes
    {
        /// <summary>
        /// Type of power up which gives extra range
        /// </summary>
       ExtraRange = 1
    }
}
