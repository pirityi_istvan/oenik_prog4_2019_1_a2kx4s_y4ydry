﻿// <copyright file="MapEntity.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Entities, that can be placed on the map
    /// </summary>
    public abstract class MapEntity
    {
        /// <summary>
        /// Gets or sets position of the entity
        /// </summary>
        public Point Position { get; set; }

        /// <summary>
        /// Gets or sets the image id of the given MapEntity
        /// </summary>
        public int Imageid { get; set; }
    }
}
