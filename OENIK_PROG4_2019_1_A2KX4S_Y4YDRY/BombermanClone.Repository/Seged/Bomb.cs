﻿// <copyright file="Bomb.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// the bomb, each player can place
    /// </summary>
    public class Bomb : MapEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Bomb"/> class.
        ///
        /// </summary>
        /// <param name="player">The player who put down the bomb</param>
        public Bomb(Player player)
        {
            this.Player = player;
            this.Position = new Point(Math.Round(player.Position.X), Math.Round(player.Position.Y));
            this.Imageid = 0;

            // bombwatch = new Stopwatch();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Bomb"/> class.
        ///
        /// </summary>
        public Bomb()
        {
            this.Imageid = 0;
        }

        /// <summary>
        /// Gets the player that placed the bomb
        /// </summary>
        public Player Player { get; }
    }
}
