﻿// <copyright file="PowerUp.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository.Seged
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Powerup class
    /// </summary>
    public class PowerUp : MapEntity
    {
        private static int imageIdStartingIndex = 2;

        /// <summary>
        /// Initializes a new instance of the <see cref="PowerUp"/> class.
        ///
        /// </summary>
        /// <param name="x">x axis</param>
        /// <param name="y">y axis</param>
        /// <param name="type">type</param>
        public PowerUp(int x, int y, PowerUpTypes type)
        {
            this.Position = new System.Windows.Point(x, y);

            this.PowerUpType = type;
            this.Imageid = (int)type + imageIdStartingIndex - 1;
        }

        /// <summary>
        /// Gets or sets type of the powerup
        /// </summary>
        public PowerUpTypes PowerUpType { get; set; }
    }
}
