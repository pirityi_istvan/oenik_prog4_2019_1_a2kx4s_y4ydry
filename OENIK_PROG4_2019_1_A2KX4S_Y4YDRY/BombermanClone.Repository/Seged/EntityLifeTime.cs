﻿// <copyright file="EntityLifeTime.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository.Seged
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Handles temporary objects
    /// </summary>
    /// <typeparam name="T">type of object</typeparam>
    public class EntityLifeTime<T>
        where T : MapEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EntityLifeTime{T}"/> class.
        ///
        /// </summary>
        /// <param name="lifeTime">Determines how long the entity will stay "alive"</param>
        /// <param name="entity">The entity</param>
        public EntityLifeTime(double lifeTime, T entity)
        {
            this.Current = 0;

            this.Max = lifeTime;
            this.Entity = entity;
        }

        /// <summary>
        /// Gets the entity
        /// </summary>
        public T Entity { get; }

        /// <summary>
        /// Gets or sets the time the entity has been alive
        /// </summary>
        public double Current { get; set; }

        /// <summary>
        /// Gets or sets max lifetime of the entity.
        /// </summary>
        public double Max { get; set; }
    }
}
