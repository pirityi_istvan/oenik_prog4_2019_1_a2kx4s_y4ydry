﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Character, that can be controleld by input
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        ///
        /// </summary>
        public Player()
        {
            this.PlayerSize = 0.5;
            this.BombPower = 1;
            this.BombsOnMap = 0;
            this.MaxBombs = 1;
            this.Alive = true;

            this.BombExplosionTime = 3;
            this.BlastTime = 1;
        }

        /// <summary>
        /// Gets or sets how long the bomb will reach
        /// </summary>
        public int BombPower { get; set; }

        /// <summary>
        /// Gets or sets the amount of bombs the player currently has on the map
        /// </summary>
        public int BombsOnMap { get; set; }

        /// <summary>
        /// Gets or sets the maximum amount of bombs the player can have on the map
        /// </summary>
        public int MaxBombs { get; set; }

        /// <summary>
        /// Gets or sets how long it takes for the bomb to explode
        /// </summary>
        public double BombExplosionTime { get; set; }

        /// <summary>
        /// Gets or sets how long the blasts will persist
        /// </summary>
        public double BlastTime { get; set; }

        /// <summary>
        /// Gets or sets position of the player
        /// </summary>
        public Point Position { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the player is alive or not
        /// </summary>
        public bool Alive { get; set; }

        /// <summary>
        /// Gets or sets the image, that is used, to represent this player
        /// </summary>
        public int Imageid { get; set; }

        /// <summary>
        /// Gets or sets the size of the player.
        /// </summary>
        public double PlayerSize { get; set; }
    }
}
