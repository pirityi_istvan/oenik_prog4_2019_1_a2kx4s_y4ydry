﻿// <copyright file="Map.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository.Seged
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// game map
    /// </summary>
    public class Map
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Map"/> class.
        ///
        /// </summary>
        public Map()
        {
            this.StartingPositions = new List<Point>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Map"/> class.
        ///
        /// </summary>
        /// <param name="map">the matrix thats the base of the map</param>
        /// <param name="startingPositions">starting positions</param>
        public Map(MapTile[,] map, List<Point> startingPositions)
        {
            this.OriginalMapState = map;
            this.OriginalMapState = this.Transpose(map);
            this.CurrentMapState = new MapTile[this.OriginalMapState.GetLength(0), this.OriginalMapState.GetLength(1)];

            // deep clone of OriginalMapState
            for (int i = 0; i < this.OriginalMapState.GetLength(0); i++)
            {
                for (int j = 0; j < this.OriginalMapState.GetLength(1); j++)
                {
                    this.CurrentMapState[i, j] = new MapTile(this.OriginalMapState[i, j].TileType);
                }
            }

            this.StartingPositions = startingPositions;
        }

        /// <summary>
        /// Gets or sets the original map state (used for reseting a match)
        /// </summary>
        public MapTile[,] OriginalMapState { get; set; }

        /// <summary>
        /// Gets or sets the current map state(keeps track of changes)
        /// </summary>
        public MapTile[,] CurrentMapState { get; set; }

        /// <summary>
        /// Gets starting position of players
        /// </summary>
        public List<Point> StartingPositions { get; private set; }

        /// <summary>
        /// Transposes a matrix
        /// </summary>
        /// <param name="matrix">matrix to transpose</param>
        /// <returns>the transposed matrix</returns>
        public MapTile[,] Transpose(MapTile[,] matrix)
        {
            int w = matrix.GetLength(0);
            int h = matrix.GetLength(1);

            MapTile[,] result = new MapTile[h, w];

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    result[j, i] = matrix[i, j];
                }
            }

            return result;
        }
    }
}
