﻿// <copyright file="Irepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BombermanClone.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using BombermanClone.Repository.Seged;

    /// <summary>
    /// Interface for the game repository
    /// </summary>
   public interface IRepository
    {
        /// <summary>
        /// Reads the map from the file, and returns it, as a 2d MapTile array
        /// </summary>
        /// <returns>The map</returns>
        Map LoadMap();
    }
}
