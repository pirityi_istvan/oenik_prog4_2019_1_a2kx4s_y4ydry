var class_bomberman_clone_1_1_repository_1_1_player =
[
    [ "Player", "class_bomberman_clone_1_1_repository_1_1_player.html#a5630f857a90a8250986fa9ac374362b5", null ],
    [ "Alive", "class_bomberman_clone_1_1_repository_1_1_player.html#a66a08e7de024fea1858501c339ae68f2", null ],
    [ "BlastTime", "class_bomberman_clone_1_1_repository_1_1_player.html#a81c29f9ecb157308e7e03c8652642cf2", null ],
    [ "BombExplosionTime", "class_bomberman_clone_1_1_repository_1_1_player.html#a45ef209326d733409ffe8e7dde1a8c43", null ],
    [ "BombPower", "class_bomberman_clone_1_1_repository_1_1_player.html#a4a805c71520f1178d70215e55f62d663", null ],
    [ "BombsOnMap", "class_bomberman_clone_1_1_repository_1_1_player.html#afc3edfadc420d229aada2b718376d403", null ],
    [ "Imageid", "class_bomberman_clone_1_1_repository_1_1_player.html#aac9337dd3164baa90896263e0e6e10f5", null ],
    [ "MaxBombs", "class_bomberman_clone_1_1_repository_1_1_player.html#a929c2d9f4de9f2ca939433239e0e0897", null ],
    [ "PlayerSize", "class_bomberman_clone_1_1_repository_1_1_player.html#a759a5b946c49955b50fd15e7b53033b7", null ],
    [ "Position", "class_bomberman_clone_1_1_repository_1_1_player.html#af0c7ce4489a7f463b06db03f109288cf", null ]
];