var class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests =
[
    [ "CanMoveDownWhenTheresNoWall", "class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests.html#a4a412d22772b2e1735a069a95b5e3842", null ],
    [ "CanMoveLeftWhenTheresNoWall", "class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests.html#abc276e53cabf9bca4b684cf715943a1b", null ],
    [ "CanMoveRightWhenTheresNoWall", "class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests.html#a289ff66c8d76aa9121d181f1d0e7d57e", null ],
    [ "CanMoveUpWhenTheresNoWall", "class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests.html#af27527878e92983abbfb41640007f089", null ],
    [ "CanOnlyMoveDownToEdgeOfWall", "class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests.html#a3baf89b5fecef0cd974549a546076c2e", null ],
    [ "CanOnlyMoveLeftToEdgeOfWall", "class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests.html#a510cba56a19c91744ee24463b20b4c32", null ],
    [ "CanOnlyMoveRightToEdgeOfWall", "class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests.html#a6630ad4f967921489da9f64e57ed2642", null ],
    [ "CanOnlyMoveUpToEdgeOfWall", "class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests.html#a7dfdb3e264a63228e07308ec7c8886af", null ],
    [ "CanPlaceMoreBombs_IfMaxBombsNotOne", "class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests.html#a9735dbb07fc2e1c2f55e174dc44f1f23", null ],
    [ "CantPlaceMoreThanOneBombInTheSameLocation", "class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests.html#a2cb45469e33d0d85309c8f3e7d32a38a", null ],
    [ "Setup", "class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests.html#a785d63094cd6a2841993b6aacc3d331c", null ]
];