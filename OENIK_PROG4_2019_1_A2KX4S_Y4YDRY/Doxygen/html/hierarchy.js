var hierarchy =
[
    [ "Application", null, [
      [ "OENIK_PROG4_2019_1_A2KX4S_Y4YDRY.App", "class_o_e_n_i_k___p_r_o_g4__2019__1___a2_k_x4_s___y4_y_d_r_y_1_1_app.html", null ],
      [ "OENIK_PROG4_2019_1_A2KX4S_Y4YDRY.App", "class_o_e_n_i_k___p_r_o_g4__2019__1___a2_k_x4_s___y4_y_d_r_y_1_1_app.html", null ],
      [ "OENIK_PROG4_2019_1_A2KX4S_Y4YDRY.App", "class_o_e_n_i_k___p_r_o_g4__2019__1___a2_k_x4_s___y4_y_d_r_y_1_1_app.html", null ]
    ] ],
    [ "ApplicationSettingsBase", null, [
      [ "OENIK_PROG4_2019_1_A2KX4S_Y4YDRY.Properties.Settings", "class_o_e_n_i_k___p_r_o_g4__2019__1___a2_k_x4_s___y4_y_d_r_y_1_1_properties_1_1_settings.html", null ]
    ] ],
    [ "BombermanClone.Repository.Seged.EntityLifeTime< T >", "class_bomberman_clone_1_1_repository_1_1_seged_1_1_entity_life_time.html", null ],
    [ "FrameworkElement", null, [
      [ "OENIK_PROG4_2019_1_A2KX4S_Y4YDRY.GameScreen", "class_o_e_n_i_k___p_r_o_g4__2019__1___a2_k_x4_s___y4_y_d_r_y_1_1_game_screen.html", null ]
    ] ],
    [ "IComponentConnector", null, [
      [ "OENIK_PROG4_2019_1_A2KX4S_Y4YDRY.MainWindow", "class_o_e_n_i_k___p_r_o_g4__2019__1___a2_k_x4_s___y4_y_d_r_y_1_1_main_window.html", null ],
      [ "OENIK_PROG4_2019_1_A2KX4S_Y4YDRY.MainWindow", "class_o_e_n_i_k___p_r_o_g4__2019__1___a2_k_x4_s___y4_y_d_r_y_1_1_main_window.html", null ]
    ] ],
    [ "BombermanClone.Logic.IGameLogic", "interface_bomberman_clone_1_1_logic_1_1_i_game_logic.html", [
      [ "BombermanClone.Logic.GameLogic", "class_bomberman_clone_1_1_logic_1_1_game_logic.html", null ]
    ] ],
    [ "BombermanClone.Repository.IGameModel", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html", [
      [ "BombermanClone.Repository.GameModel", "class_bomberman_clone_1_1_repository_1_1_game_model.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "BombermanClone.Repository.IRepository", "interface_bomberman_clone_1_1_repository_1_1_i_repository.html", [
      [ "BombermanClone.Repository.GameRepository", "class_bomberman_clone_1_1_repository_1_1_game_repository.html", null ]
    ] ],
    [ "BombermanClone.Logic.Tests.LogicTests", "class_bomberman_clone_1_1_logic_1_1_tests_1_1_logic_tests.html", null ],
    [ "BombermanClone.Repository.Seged.Map", "class_bomberman_clone_1_1_repository_1_1_seged_1_1_map.html", null ],
    [ "BombermanClone.Repository.MapEntity", "class_bomberman_clone_1_1_repository_1_1_map_entity.html", [
      [ "BombermanClone.Repository.Bomb", "class_bomberman_clone_1_1_repository_1_1_bomb.html", null ],
      [ "BombermanClone.Repository.Seged.Blast", "class_bomberman_clone_1_1_repository_1_1_seged_1_1_blast.html", null ],
      [ "BombermanClone.Repository.Seged.PowerUp", "class_bomberman_clone_1_1_repository_1_1_seged_1_1_power_up.html", null ]
    ] ],
    [ "ObservableObject", null, [
      [ "BombermanClone.Repository.Seged.MapTile", "class_bomberman_clone_1_1_repository_1_1_seged_1_1_map_tile.html", null ]
    ] ],
    [ "BombermanClone.Repository.Player", "class_bomberman_clone_1_1_repository_1_1_player.html", null ],
    [ "OENIK_PROG4_2019_1_A2KX4S_Y4YDRY.Properties.Resources", "class_o_e_n_i_k___p_r_o_g4__2019__1___a2_k_x4_s___y4_y_d_r_y_1_1_properties_1_1_resources.html", null ],
    [ "Window", null, [
      [ "OENIK_PROG4_2019_1_A2KX4S_Y4YDRY.MainWindow", "class_o_e_n_i_k___p_r_o_g4__2019__1___a2_k_x4_s___y4_y_d_r_y_1_1_main_window.html", null ],
      [ "OENIK_PROG4_2019_1_A2KX4S_Y4YDRY.MainWindow", "class_o_e_n_i_k___p_r_o_g4__2019__1___a2_k_x4_s___y4_y_d_r_y_1_1_main_window.html", null ],
      [ "OENIK_PROG4_2019_1_A2KX4S_Y4YDRY.MainWindow", "class_o_e_n_i_k___p_r_o_g4__2019__1___a2_k_x4_s___y4_y_d_r_y_1_1_main_window.html", null ]
    ] ]
];