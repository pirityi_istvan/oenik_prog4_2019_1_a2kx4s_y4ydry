var interface_bomberman_clone_1_1_repository_1_1_i_game_model =
[
    [ "BlastMap", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#a1c5f5822725ff3aa8b5516006c385ee5", null ],
    [ "Blasts", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#ad35c5bc279963ef0b55781cf89e322e6", null ],
    [ "Bombs", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#af98f49886193526c83899a8ff125125b", null ],
    [ "CurrentRound", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#ae4413bbc7c26fcf538480ae2445baeac", null ],
    [ "InteractableEntities", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#a00c77d2c70b6b995ec3368b5a08348c0", null ],
    [ "MapObjects", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#a03bbd1a1524c66b7c763a2d5c127ba53", null ],
    [ "MatchLength", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#a0107cd64f159d0ebffe256e1c7be9e38", null ],
    [ "MatchMap", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#a87a84e512dd31c1db8fb603506c7261e", null ],
    [ "MatchTime", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#aa9f35f98537cb0392812e91172eff0c5", null ],
    [ "Players", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#a18f2f659548d259acf78e30f7d005220", null ],
    [ "PowerUpChance", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#a23be5adff72e59b28125b964c404a168", null ],
    [ "Scores", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#a22071391eab8b6d473a97fccf4d94f85", null ],
    [ "TileSize", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#a979c99d883f513d4fcfe5aef37bad4e8", null ],
    [ "Timer", "interface_bomberman_clone_1_1_repository_1_1_i_game_model.html#ab18171a62f047e9718de9873c6f8b130", null ]
];